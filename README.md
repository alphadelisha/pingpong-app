# pingpong-app built in with node js

## How to start without docker
- yarn install
- yarn run start


## How to start with docker

1. Clone pingpong-app repo into your local machine
- git clone https://gitlab.com/alphadelisha/pingpong-app.git

2. Open pingpong-app and Set env file
- cd pingpong-app && ./setenv.sh

3. Build image from pingpong-app
- docker build -t pingpong-server .



