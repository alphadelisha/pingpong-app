#!/bin/bash

# Copy .env.example .env

echo "1. Copy .env.example into .env"
cp .env.example .env

echo ""
echo "Done..."
echo ""

# Assign the filename
filename=".env"

echo "2. Set app name"
# Search for old app-name
read -p "Enter the old app-name: " appNameSearch

# Replace the old app-name
read -p "Enter the new app-name: " appNameReplace

if [[ $appNameSearch != "" && $appNameReplace != "" ]]; then
sed -i "s/$appNameSearch/$appNameReplace/" $filename
fi

echo ""
echo "Done..."
echo ""

echo "3. Set app port"
# Search for old app-port
read -p "Enter the old app-port: " appPortSearch

# Replace the old app-port
read -p "Enter the new app-port: " appPortReplace

if [[ $appPortSearch != "" && $appPortReplace != "" ]]; then
sed -i "s/$appPortSearch/$appPortReplace/" $filename
fi

echo ""
echo "Done..."
echo ""
